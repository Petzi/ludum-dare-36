///print_list(list)

var list, i, size;
list = argument0;
size = ds_list_size(list)

for (i=0; i<size; i++) {
    console_write("#" + string(i) + ". " + list[|i])
}

if size == 0
    console_write("#Nothing")
