///mirror_update(plus)

var num = argument0

with (obj_pc) {

    mirror_level = (mirror_level + num) mod 4
    
    switch (mirror_level) {
        case 0 : mirror_x = 1;
                 mirror_y = 1;
                 break;
        
        case 1 : mirror_x = -1;
                 mirror_y = 1;
                 break;
                 
        case 2 : mirror_x = -1;
                 mirror_y = -1;
                 break;
                 
        case 3 : mirror_x = 1;
                 mirror_y = -1;
                 break;
    }
}
