///insert(obj_floppy)

var floppy = argument0;

with (floppy) {
    in_slot = 1
    obj_slot.occupant = id
    engage(obj_crank_handle)
    
    image_yscale_apparent = 0.5
    depth = 45 //floor(abs(object_get_depth(obj_pc_top) + object_get_depth(obj_pc_bottom)))
    
    gravity = 0
    vspeed = 0
    hspeed = 0
    
    in_max_y = obj_slot.y + sprite_get_yoffset(sprite_index)*image_yscale_apparent
    in_min_y = in_max_y - 40
    
    x = obj_slot.x
    y = in_max_y
}
