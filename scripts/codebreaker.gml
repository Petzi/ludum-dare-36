///codebreaker()

var guess = string_upper(string_letters(last_input));

console_write("#" + guess)

var len = string_length(key)

if string_length(guess) != len {
    console_write(" ** It has " + string(len) + " letters!")
    exit
}

var i, g, k, rights, out_of_places, char;
g = guess
k = key
rights = 0
out_of_places = 0

for (i=1; i<=len; i++) {
    char = sget(g, i)

    if char == sget(k, i) {
        rights++
        g = string_delete(g, i, 1)
        k = string_delete(k, i, 1)
        g = string_insert("*", g, i)
        k = string_insert("_", k, i)
    }
}

var char_k_pos;

for (i=1; i<=len; i++) {
    char = sget(g, i)
    char_k_pos = string_pos(char, k)

    if char_k_pos > 0 {
        out_of_places++
        k = string_replace(k, char, "_")
    }
}

var wrong = len - rights - out_of_places

if rights == len {
    console_write("#You won! Congratulations, the word was " + guess)
    ask("#Play again?", q_play_again)
}
else
    console_write(" ** " +string(rights) + " right, "
                    + string(out_of_places) + " out of place, "
                    + string(wrong) + " wrong.")
