///unpause()

with (obj_holdable)
    active = 1
    
with (obj_quit)
    active = 1
    
with (obj_yes) instance_destroy()
with (obj_box) instance_destroy()
with (obj_restart) instance_destroy()
with (obj_no) instance_destroy()
