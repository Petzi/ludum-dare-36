///grab(obj_holdable)

var inst = argument0;

if obj_control.mouse_holding != noone {
    show_debug_message("Already holding something")
    letgo()
}

inst.being_held = 1
obj_control.mouse_holding = inst

inst.offset_x = inst.x - mouse_x
inst.offset_y = inst.y - mouse_y

inst.gravity = 0
inst.speed = 0
