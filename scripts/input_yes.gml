///input_yes()

var txt = last_input;
txt = string_lower(txt)

if txt == "yes" or txt == "y"
    return 1
else if txt == "no" or txt == "n"
    return 0
else {
    console_write("#Error: please input 'yes' or 'no'")
    return -1
}
