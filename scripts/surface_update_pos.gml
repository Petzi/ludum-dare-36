///surface_update_pos()

var rotation_level;

switch (rotation) {
    case 0 :
        rotation_level = 0;
        break;
        
    case 90 :
        rotation_level = 1;
        break;
        
    case 180 :
        rotation_level = 2;
        break;
        
    case 270 :
        rotation_level = 3;
        break;
}

var corner_level = (mirror_level + rotation_level) mod 4;
var corner;

switch (corner_level) {
    case 0 :
        corner = obj_screen_corner_tl.id;
        text_halign = fa_left;
        text_valign = fa_top;
        break;
        
    case 1 :
        corner = obj_screen_corner_tr.id;
        text_halign = fa_right;
        text_valign = fa_top;
        break;
        
    case 2 :
        corner = obj_screen_corner_br.id;
        text_halign = fa_right;
        text_valign = fa_bottom;
        break;
        
    case 3 :
        corner = obj_screen_corner_bl.id;
        text_halign = fa_left;
        text_valign = fa_bottom;
        break;
}

surf_x = corner.x
surf_y = corner.y
