///engage(obj_crank_handle)

with (argument0) {

    if engaged
        show_debug_message("Already engaged!")
        
    engaged = 1
    max_angle = angle
    min_angle = max_angle - CRANK_TURNS*360
}
