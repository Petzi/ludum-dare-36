///load_program(prog)

var prog = argument0;

if ds_list_find_index(programs, prog) == -1 {
    ds_list_add(programs, prog)
    console_write("#Program " + prog + " is now in memory.")
}
else
    console_write("#Program " + prog + " is already in memory.")
