///pause()

with (obj_holdable)
    active = 0
    
with (obj_quit)
    active = 0
