///color_update(plus)

var num = argument0

with (obj_pc) {

    color_level = (color_level + num) mod 3
    
    switch (color_level) {
        case 0 : color = c_lime;
                 break;
        
        case 1 : color = c_aqua;
                 break;
                 
        case 2 : color = c_wubly;
                 break;
    }
}
