///readdrive()

if floppy_in_slot == obj_os.id
    console_write("#Wubly is already running.")

if floppy_in_slot == obj_network.id {
    ask("#1 program found in drive. Load to memory?", q_load_network)
}

if floppy_in_slot == obj_game.id {
    ask("#1 program found in drive. Load to memory?", q_load_game)
}
