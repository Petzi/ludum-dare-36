///toggle_light(obj_light)

var inst = argument0;

if inst.on
    inst.on = false
else
    inst.on = true
    
if inst.image_index
    inst.image_index = 0
else
    inst.image_index = 1
