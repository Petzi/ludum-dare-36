///letgo()

var inst = obj_control.mouse_holding;

if obj_control.mouse_holding == noone {
    show_debug_message("Nothing to let go")
    return -1
}

inst.being_held = 0
obj_control.mouse_holding = noone

if object_is_ancestor(inst.object_index, obj_draggable) and not inst.in_slot {
    with (inst)
        var col = collision_point_list(x+sticky_x, y+sticky_y, stick_to, 0, 1);
    
    if col == noone or global.mouse_vspeed < 0 {
        inst.hspeed = global.mouse_hspeed
        inst.vspeed = global.mouse_vspeed
        inst.speed = min(inst.speed, MAX_SPEED)
//        inst.friction = 0.02
        inst.gravity_direction = 270
        inst.gravity = 0.5 + friction
        inst.found_ground = 0
    }
    else
        ds_list_destroy(col)
}
