///console_write(string)

console += argument0

var count = string_count("#", console);

if count > MAX_CONSOLE_LINES - 2 {
    do {
        var pos = string_pos("#", console);
        console = string_delete(console, 1, pos)
        count = string_count("#", console)
    }
    until (count <= MAX_CONSOLE_LINES - 2)
}
