///restart_pc()

with (obj_pc) {
    on = false
    os = 0
    updated = 0
    
    highscore = 0
    
    ds_list_clear(programs)
    ds_list_clear(files)
    
    floppy_in_slot = noone
    
    do {
        frequency = irandom_range(MIN_FREQ, MAX_FREQ)
    }
    until (frequency != 197)
    
    
    console = "Error: No Operating System."
    input = PREBOOT
    last_input = ""
    last_input_used = 1
    
    alarm[0] = -1
    alarm[1] = -1
    alarm[2] = -1
    alarm[3] = -1
    alarm[4] = -1
    alarm[5] = -1
    alarm[6] = -1
    alarm[7] = -1
    alarm[8] = -1
    alarm[9] = -1
    alarm[10] = -1
    alarm[11] = -1
}
