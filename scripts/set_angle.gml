///set_angle(obj_turnable, angle)

with (argument0) {
    angle = argument1
    image_angle = angle - base_angle
    slave.image_angle = angle - base_angle
}
