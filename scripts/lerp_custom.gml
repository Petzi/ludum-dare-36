///lerp_custom(x0, x1, y0, y1, xpoint)
/* Linear interpolation where amount is not necessarily a percentage
*/

var x0, y0, x1, y1, xpoint;
x0 = argument0
x1 = argument1
y0 = argument2
y1 = argument3
xpoint = argument4

return y0 + (y1-y0) * (xpoint-x0)/(x1-x0)
