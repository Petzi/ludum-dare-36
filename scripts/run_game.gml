///run_game()

if !updated {
    ask("#CODEBREAKER incompatible with WublyOS 1.2 gameplay drivers.#Update?", q_update)
}
else
    ask("#Play CODEBREAKER?", q_play)
