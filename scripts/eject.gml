///eject()

var floppy = obj_slot.occupant;

floppy.in_slot = 0
obj_slot.occupant = noone
disengage(obj_crank_handle)

floppy.depth = object_get_depth(obj_floppy)
floppy.image_yscale_apparent = 1
