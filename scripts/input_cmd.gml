///input_cmd()

var num;

if string_delete(last_input, 5, 9999) == "run " {
    var prog = string_delete(last_input, 1, 4)
    var error = 0

    if prog == string_digits(prog) {
        num = real(prog)
        
        if num >= ds_list_size(programs)
            error = 1
    }
    else
        error = 1
    
    if error {
        console_write("#No such program '" + prog + "'.")
        exit
    }
        
    switch (programs[|num]) {
        case PROG_NETWORK : 
            run_network()
            break;
            
        case PROG_GAME : 
            run_game()
            break;
    }
    
    exit
}
    

switch (last_input) {
    case "" :
        break;

    case "run" :
        console_write("#Usage: $run <program NUMBER>");
        break;

    case "clear" :
        console = "";
        break;
        
    case "info" :
        console_write("#Wubly OS 1.2 is brought to you by 2CSoft")
        break;
        
    case "help" :
        console_write("#clear#info#run#programs#readdrive");
        break;
        
    case "readdrive" :
        readdrive();
        break;
    
    case "programs" :
        print_list(programs);
        break;
        
    /*case "files" :
        print_list(files);
        break;*/
        
    default :
        console_write("#No such command '" + last_input + "'.");
        break;
}
