///disengage(obj_crank_handle)

with (argument0) {

    if !engaged
        show_debug_message("Already disengaged!")
        
    engaged = 0
}
